# Command line MS TTS interface

based on example code from
https://blogs.msdn.microsoft.com/matthew_van_eerde/2013/03/13/grabbing-the-output-of-the-microsoft-speech-api-text-to-speech-engine-as-audio-data/

and getopt from
https://raw.githubusercontent.com/iotivity/iotivity/master/resource/c_common/windows/

## Usage
cmdTTS -u "phrase" [-f <filename>] [-t] [-v <voice>]\n"

runs phrase through text-to-speech engine\n"
if -f <filename> is specified, writes to .wav file\n"
if -t is specified, print utterance length in seconds\n"
if neither is specified, plays to default output\n"
-v <voice> selects TTS voice\n"
-h show this help\n"

## Voices
- Available voices at in the registry under
```HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices```

- In the Control Panel, search for �speech� and then click on the Change text to speech settings.

### Problem with CereProc William
- If after installation, CereProc William is not listed, run the "Enable CereProc William Windows 10.reg" registry patch

example voice names:
```Microsoft Zira Desktop```
```CereVoice Adam - English (East Coast America)```


