// main.cpp

#include <windows.h>
#include <stdio.h>
#include <sapi.h>
#include <shlwapi.h>
#include <sapi.h>  
#include <sphelper.h>  
#include <atlbase.h>
#include <string>
#include <iostream>

#include "getopt.h"

#define LOG(format, ...) wprintf(format L"\n", __VA_ARGS__)
#define ERR(format, ...) wprintf(L"ERROR:" format L"\n", __VA_ARGS__)

#define CHANNELS 1
#define SAMPLE_RATE 8000
#define BIT_DEPTH 16
#define FRAMES_PER_PASS 4

class CoUninitializeOnExit {
public:
	CoUninitializeOnExit() {}
	~CoUninitializeOnExit() { CoUninitialize(); }
};

class ReleaseOnExit {
public:
	ReleaseOnExit(IUnknown *p) : m_p(p) {}
	~ReleaseOnExit() {
		if (m_p) {
			m_p->Release();
		}
	}
private:
	IUnknown *m_p;
};

void usage() {
	LOG(
		L"usage: cmdTTS -u \"phrase\" [-f <filename>] [-t] [-v <voice>]\n"
		L"\truns phrase through text-to-speech engine\n"
		L"\tif -f <filename> is specified, writes to .wav file\n"
		L"\tif -t is specified, print utterance length in seconds\n"
		L"\tif neither is specified, plays to default output\n"
		L"\t-v <voice> selects TTS voice\n"
		L"\t-h show this help\n"
	);

}


int _cdecl wmain(int argc, LPCWSTR argv[]) {
	LPCWSTR cpUtterance = NULL;
	LPCWSTR cpFile = NULL;
	LPCWSTR cpVoice = NULL;
	int c;
	int bTime = 0;
	int opterr = 0;

	// : means there is a parameter for the flag
	// | means there is NOT parameter for the flag (e.g. -h)
	while ((c = getopt(argc, argv, "u:f:t|v:h|")) != -1) {
		switch (c) {
		case 'u':
			cpUtterance = optarg;
			break;
		case 'f':
			cpFile = optarg;
			break;
		case 't':
			bTime = -1;
			break;
		case 'v':
			cpVoice = optarg;
			break;
		case 'h':
			usage();
			exit(0);
			break;
		default:
			usage();
			exit(-1);
		}
	}
	if (!cpUtterance) {
		ERR(L"Missing input: Utterance");
		usage();
		exit(-1);
	}

	//wprintf(L"utterance: %s, Time: %d, File: %s, Voice: %s\n", 	cpUtterance, bTime, cpFile, cpVoice);

	enum Output {
		Speakers,
		File,
		Stream
	};

	Output where;
	LPCWSTR file = nullptr;

	if (!cpFile) {
		where = Speakers;
	}

	if (cpFile) {
		where = File;
		file = cpFile;
	}

	if (bTime) {
		where = Stream;
	}

	LPCWSTR text = cpUtterance;

	HRESULT hr = CoInitialize(nullptr);
	if (FAILED(hr)) {
		ERR(L"CoInitialize failed: hr = 0x%08x", hr);
		return -__LINE__;
	}


	CoUninitializeOnExit cuoe;

	ISpVoice *pSpVoice = nullptr;
	hr = CoCreateInstance(CLSID_SpVoice, nullptr, CLSCTX_ALL, __uuidof(ISpVoice), (void**)&pSpVoice);

	if (FAILED(hr)) {
		ERR(L"CoCreateInstance(ISpVoice) failed: hr = 0x%08x", hr);
		return -__LINE__;
	}


	//	cpVoice = L"Microsoft Zira Desktop";
	//	cpVoice = NULL;
	if (cpVoice) {

		CComPtr<ISpObjectToken>        cpVoiceToken;
		CComPtr<IEnumSpObjectTokens>   cpEnum;

		if (SUCCEEDED(hr)) {
			//hr = SpEnumTokens(SPCAT_VOICES, L"Name=Microsoft Zira Desktop", NULL, &cpEnum);
			hr = SpEnumTokens(SPCAT_VOICES, (L"name=" + std::wstring(cpVoice)).c_str(), NULL, &cpEnum);
		}

		if (SUCCEEDED(hr)) {
			hr = cpEnum->Next(1, &cpVoiceToken, NULL);
		}

		if (SUCCEEDED(hr)) {
			hr = pSpVoice->SetVoice(cpVoiceToken);
		}

		cpVoiceToken.Release();
		cpEnum.Release();
	}


	ReleaseOnExit rSpVoice(pSpVoice);
	WAVEFORMATEX fmt = {};
	fmt.wFormatTag = WAVE_FORMAT_PCM;
	fmt.nChannels = CHANNELS;
	fmt.nSamplesPerSec = SAMPLE_RATE;
	// nAvgBytesPerSec is calculated
	// nBlockAlign is calculated
	fmt.wBitsPerSample = BIT_DEPTH;
	fmt.cbSize = 12;
	fmt.nBlockAlign = fmt.nChannels * fmt.wBitsPerSample / 8;
	fmt.nAvgBytesPerSec = fmt.nSamplesPerSec * fmt.nBlockAlign;

	IStream *pStream = NULL;

	switch (where) {

	case Speakers:
		break;
	case File:
	case Stream: 
		ISpStream *pSpStream = nullptr;
		hr = CoCreateInstance(CLSID_SpStream, nullptr, CLSCTX_ALL, __uuidof(ISpStream), (void**)&pSpStream);

		if (FAILED(hr)) {
			ERR(L"CoCreateInstance(ISpVoice) failed: hr = 0x%08x", hr);
			return -__LINE__;
		}
		ReleaseOnExit rSpStream(pSpStream);

		if (File == where) {
			hr = pSpStream->BindToFile(file, SPFM_CREATE_ALWAYS, &SPDFID_WaveFormatEx, &fmt, 0);
			if (FAILED(hr)) {
				ERR(L"ISpStream::BindToFile failed: hr = 0x%08x", hr);
				return -__LINE__;
			}
		}
		else {
			// stream
			pStream = SHCreateMemStream(NULL, 0);
			if (nullptr == pStream) {
				ERR(L"SHCreateMemStream failed");
				return -__LINE__;
			}

			hr = pSpStream->SetBaseStream(pStream, SPDFID_WaveFormatEx, &fmt);
			if (FAILED(hr)) {
				ERR(L"ISpStream::SetBaseStream failed: hr = 0x%08x", hr);
				return -__LINE__;
			}
		}

		hr = pSpVoice->SetOutput(pSpStream, TRUE);
		if (FAILED(hr)) {
			ERR(L"ISpVoice::SetOutput failed: hr = 0x%08x", hr);
			return -__LINE__;
		}
		break;
	
	}

	ReleaseOnExit rStream(pStream);

	ULONG stream = 0;
	hr = pSpVoice->Speak(text, SPF_DEFAULT, &stream); // this works we replace " with '...
	//hr = pSpVoice->Speak(L"c:\\filename.xml", SPF_IS_FILENAME, NULL); // this works for SSML...

	if (FAILED(hr)) {
		ERR(L"ISpVoice::Speak failed: hr = 0x%08x", hr);
		return -__LINE__;
	}
	//LOG(L"Stream is %u", stream);

	if (Stream == where) {
		// the stream has stereo 16-bit data
		// read 128 samples at a time
		static_assert(BIT_DEPTH == 16, "assuming int16");
		//		INT16 chunk[128 * CHANNELS];
		ULONG read = 0;

		// SAPI wrote to the stream and filled it up
		// but the position is still at the end
		// so we need to rewind back to the beginning
		// before we can read it
		LARGE_INTEGER zero = { 0 };
		hr = pStream->Seek(zero, STREAM_SEEK_SET, NULL);
		if (FAILED(hr)) {
			ERR(L"IStream::Seek failed: hr = 0x%08x", hr);
			return -__LINE__;
		}

		STATSTG stats;
		pStream->Stat(&stats, STATFLAG_DEFAULT);
		double duration = (double)stats.cbSize.QuadPart / (FRAMES_PER_PASS * SAMPLE_RATE) * 2.0; // 2.0-> CHANNEL = 1
		wprintf(L"%f\n", duration);


#ifdef ENABLED
		for (bool eos = false; !eos; ) {
			hr = pStream->Read(chunk, sizeof(chunk), &read);
			if (FAILED(hr)) {
				ERR(L"IStream::Read failed: hr = 0x%08x", hr);
				return -__LINE__;
			}

			// LOG(L"IStream::Read returned 0x%08x and read %u bytes", hr, read);
			// Be careful - some stream implementations signal EOS differently
			eos = (S_FALSE == hr);

			if (0 != read % fmt.nBlockAlign) {
				ERR(L"IStream::Read returned a non-aligned chunk: %u bytes", read);
				return -__LINE__;
			}

			int i = 0;
			ULONG logged = 0;
			while (logged < read) {
				ULONG framesLeft = (read - logged) / fmt.nBlockAlign;
				ULONG framesLoggedThisPass = framesLeft;

				if (framesLeft > FRAMES_PER_PASS) {
					framesLoggedThisPass = FRAMES_PER_PASS;
				}

				//				static_assert(CHANNELS == 2, "assuming stereo");
				switch (framesLoggedThisPass) {
				case 1:
					LOG(L"% 8d % 8d", chunk[i], chunk[i + 1]);
					break;

				case 2:
					LOG(L"% 8d % 8d; % 8d % 8d",
						chunk[i], chunk[i + 1],
						chunk[i + 2], chunk[i + 3]
					);
					break;

				case 3:
					LOG(L"% 8d % 8d; % 8d % 8d; % 8d % 8d",
						chunk[i], chunk[i + 1],
						chunk[i + 2], chunk[i + 3],
						chunk[i + 4], chunk[i + 5]
					);
					break;

					static_assert(FRAMES_PER_PASS == 4, "assuming four frames per pass");
				case 4:
					LOG(L"% 8d % 8d; % 8d % 8d; % 8d % 8d; % 8d % 8d",
						chunk[i], chunk[i + 1],
						chunk[i + 2], chunk[i + 3],
						chunk[i + 4], chunk[i + 5],
						chunk[i + 6], chunk[i + 7]
					);
					break;
				}

				i += framesLoggedThisPass * CHANNELS;
				logged += framesLoggedThisPass * fmt.nBlockAlign;
			}
		}
#endif
	}
	return 0;
}
