import time
import timeit
import subprocess

SAY="x64\\Release\\cmdTTS.exe";

prosody_rate = 1.0

voice = 'Microsoft David Desktop'
voice = 'Microsoft Zira Desktop'
voice = 'CereVoice Adam - English (East Coast America)'
voice = "CereVoice William - English (England)"
#voice = ""
#print "calc_duration, ut_duration"
f = open('ut.txt', 'r')
x = f.readlines()
for l in x:
    l = l.strip('\n')
    utProsody = "<prosody rate='" + str(prosody_rate) + "' volume='50'>" + l + "</prosody>"
    ut = "<speak version='1.0' xmlns='http://www.w3.org/2001/10/synthesis' xml:lang='en-US'>" + utProsody + "</speak>"
    cmd = SAY + " -u \"" + ut + "\" -t -v \"" + voice + "\""
#    print(cmd)
    start_time = timeit.default_timer()
    duration_voice =  float(subprocess.check_output(cmd, shell=False))
    elapsed_voice = timeit.default_timer() - start_time

    ut = "<speak version='1.0'>" + l + "</speak>"
    cmd = SAY + " -u \"" + ut + "\" -t"
#    print(cmd)
    start_time = timeit.default_timer()
    duration_nv =  float(subprocess.check_output(cmd, shell=False))
    elapsed_nv = timeit.default_timer() - start_time

#    d2 = duration_nv * 1.12518 - 0.325647
    d2 = duration_nv * 1.116 - 1.155
    print(str(duration_voice) + ", " + str(duration_nv) + ", " + str(d2) + ", " + str(duration_voice-d2) + ", " + str(duration_voice/d2))
#    print(str(elapsed_voice) + ", " + str(elapsed_nv))
