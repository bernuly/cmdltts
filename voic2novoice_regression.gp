# calculate regression from voice-less data
f(x) = a*x + b
fit f(x) 'data.csv' u 1:2  via a, b
plot 'data.csv' using 1:2  lw 10, 'data.csv' using 1:3 lw 10, f(x) lw 10

# corrected data
plot 'data.csv' using 1:($2*1/a-b)  lw 10


